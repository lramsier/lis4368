> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Development

## Leah Ramsier

### Assignment 3 Requirements:

### Deliverables:
1. Entity Relationship Diagram (ERD)
2. Include data (at least 10 records each table)
3. Provide Bitbucket read-only access to a3 repo (Language SQL), *must* include README.md,
using Markdown syntax, and include links to *all* of the following files (from README.md):    
    - a. docs folder: a3.mwb, and a3.sql
    - b. img folder: a3.png (export a3.mwb file as a3.png)
    - c. README.md (*MUST* display a3.png ERD)
4. Blackboard Links: a3 Bitbucket repo

#### Assignment Image:

*PNG file of ERD*:

![ERD](img/a3.png)

### Assignment Links:

- [SQL file](docs/a3.sql "A3 SQl script")
- [MWB file](docs/a3.mwb "A3 ERD in MWB format")
- [ERD](img/a3.png)
