> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Development

## Leah Ramsier

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (Chs 1-4)

#### README.md file should include the following items:

#### Git commands w/short descriptions:

1. git init - create an empty Git repository or reinitialize an existing one
2. git status - show the working tree status
3. git add - add file contents to the index
4. git commit - record changes to the repository
5. git push - update remote refs along with associated objects
6. git pull - fetch from and integrate with another repository or a local branch
7. git merge - join two or more development histories together

[Git command references](https://git-scm.com/docs "git command references")

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)


*Screenshot of running http://localhost:9999*:

![Tomcat Installation Screenshot](img/tomcat.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/lramsier/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/lramsier/myteamquotes/ "My Team Quotes Tutorial")
