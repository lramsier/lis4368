# LIS4368 Advanced Web Applications Development

## Leah Ramsier

### Assignment 2 Requirements:

*Three Parts:*

1. Install MySQL
2. Complete [tutorial](https://www.ntu.edu.sg/home/ehchua/programming/howto/Tomcat_HowTo.html)
3. Chapter Questions (Chs 5-6)

#### README.md file should include the following items:


#### Assignment Screenshot:

*Screenshot of query results*:

![Screenshot of query results](IMG/query.png)


#### Assignment Links:


- [http://localhost:9999/hello/](http://localhost:9999/hello/ "Hello!")
- [http://localhost:9999/hello/index.html](http://localhost:9999/hello/index.html "Hello Home!")
- [http://localhost:9999/hello/sayhello](http://localhost:9999/hello/sayhello "Say Hello!")
- [http://localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html "Querybook!")
- [http://localhost:9999/hello/sayhi](http://localhost:9999/hello/sayhi "Say Hi!")

