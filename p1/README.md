> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Development

## Leah Ramsier

### Project 1 Requirements:

### Deliverables:
1. Provide Bitbucket read-only access to p1 repo, *must* include README.md, using Markdown
syntax.
2. Blackboard Links: p1 Bitbucket repo

### Assignment Links:

[P1](http://localhost:9999/repos/lis4368p1/index.jsp "Project 1 Form")

#### Assignment Image:

*PNG file of Project*:

![Project](img/p1.png)



